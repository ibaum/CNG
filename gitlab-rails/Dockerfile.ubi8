ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITLAB_USER=git
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf ${DNF_OPTS} install -by --disablerepo="*" --enablerepo="*ubi-8*" --nodocs libicu tzdata uuid \
    && adduser -m ${GITLAB_USER} \
    && mkdir -p ${GITLAB_DATA}/{.upgrade-status,data,repo,config} \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} ${GITLAB_DATA} \
    && chmod -R ug+rwX,o-rwx ${GITLAB_DATA}/repo \
    && chmod -R ug-s ${GITLAB_DATA}/repo

ADD gitlab-rails-ee.tar.gz /

COPY scripts/ /scripts

RUN chown -R ${GITLAB_USER}:${GITLAB_USER} /scripts /srv/gitlab \
    && mv /srv/gitlab/log /var/log/gitlab \
    && ln -s /var/log/gitlab /srv/gitlab/log \
    && cd /srv/gitlab \
    && mkdir -p public/uploads \
    && chmod o-rwx config/database.yml \
    && chmod 0600 config/secrets.yml \
    && chmod -R u+rwX builds/ shared/artifacts/ \
    && chmod -R ug+rwX shared/pages/ \
    && chmod 0700 public/uploads \
    && mkdir /home/git/gitlab-shell \
    && chown ${GITLAB_USER}:${GITLAB_USER} /home/git/gitlab-shell \
    && ln -s /srv/gitlab/GITLAB_SHELL_VERSION /home/git/gitlab-shell/VERSION \
    && sed -e '/host: localhost/d' -e '/port: 80/d' -i config/gitlab.yml \
    && sed -e "s/# user:.*/user: ${GITLAB_USER}/" -e "s:/home/git/repositories:${GITLAB_DATA}/repo:" -i config/gitlab.yml

ENV RAILS_ENV=production
ENV EXECJS_RUNTIME=Disabled
ENV CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab/config
ENV UPGRADE_STATUS_DIR=${GITLAB_DATA}/.upgrade-status

# Generate bootsnap cache
RUN echo "Generating bootsnap cache"; \
    cd /srv/gitlab && \
    su ${GITLAB_USER} -c "mkdir /srv/gitlab/tmp/"  && \
    su ${GITLAB_USER} -c "ENABLE_BOOTSNAP=1 bin/rails runner 'exit 0'" && \
    du -hs /srv/gitlab/tmp/cache/bootsnap-* ;
# exit code of this command will be that of `du`

VOLUME ${GITLAB_DATA} /var/log
